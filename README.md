# Dependencies
This project has the following dependencies:
* Docker
* Docker-compose
* [NVIDIA Container Runtime](https://github.com/NVIDIA/nvidia-container-runtime#installation)

# DAGO Environment
Execute:
* `./script/setup.sh` verifies dependencies, builds the docker images and initializes the database.
* `./script/run.sh` starts the docker containers.
* `./script/cleanup.sh` deletes all DAGO-containers and -images.
  *Note:* Volumes are not deleted during the clean-up process!

Sub-Scripts:
* `./scripts/config.sh` contains all required environment variables for bash and docker.
* `./scripts/init_postgres.sh` initializes the database.


## Setup and run project
```
$ ./script/setup.sh
$ ./script/run.sh
```

## Test Data
The data used during testing is available as a database dump in 'data/db.dump'.
To import the database dump execute the following commands:
```
$ docker container cp data/db.dump database:/root/
$ docker container exec -it psql -U dago dago < /root/db.dump
```

NOTE:
This only works for an empty database!
Thus, these steps must be executed before `script/setup.sh` has been run.
The following commands allow to prepare the test-data before setting up and running the DAGo environment:
```
$ source ./script/config.sh
$ docker-compose -f docker/docker-compose.yaml up database

# After the database dump has been imported
$ docker-compose -f docker/docker-compose.yaml stop database
```
