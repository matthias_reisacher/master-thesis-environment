#!/usr/bin/env bash
set -o nounset
#set -o xtrace

cd $(dirname "$0")
source config.sh

die() {
    echo "$@" >&2
    exit 1
}

build_image() {
    docker image build -t $1 $2
}

# Check if all required repositories can be found
[[ ! -d ${REPO_AUTOFILL} ]] && die "Error: Could not find '${REPO_AUTOFILL}'"

# Remove old instances
docker container rm -f dago-autofill 2> /dev/null

# Build images
build_image "${IMAGE_RUST}:${IMAGE_VERSION}" "${DOCKER_AUTOFILL}"
