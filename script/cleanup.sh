#!/usr/bin/env bash
set -o nounset
#set -o xtrace

cd $(dirname "$0")
source config.sh

delete_image() {
    [[ -n $(docker image ls -q "$1") ]] && docker image rm "$1"
}

docker container rm -f dago-layout 2> /dev/null
docker container rm -f dago-autofill 2> /dev/null
docker-compose -f "${DOCKER}/docker-compose.yml" down

delete_image "${IMAGE_JAVA}:${IMAGE_VERSION}"
delete_image "${IMAGE_RUST}:${IMAGE_VERSION}"
delete_image "${IMAGE_CPP}:${IMAGE_VERSION}"
delete_image "${IMAGE_FRONT_PROXY}:${IMAGE_VERSION}"
delete_image "${IMAGE_DATABASE}:${IMAGE_VERSION}"
delete_image "${IMAGE_CACHE}:${IMAGE_VERSION}"
