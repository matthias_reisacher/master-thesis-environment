#!/usr/bin/env bash
pushd . > /dev/null

cd "$(pwd)/$(dirname $BASH_SOURCE)"
SRC_ROOT=$(cd ../..; pwd)

export REPO_FRONTEND="${SRC_ROOT}/master-thesis-frontend"
export REPO_BACKEND="${SRC_ROOT}/master-thesis-backend"
export REPO_PREPROCESSOR="${SRC_ROOT}/master-thesis-backend-preprocessor"
export REPO_SEQUENCER="${SRC_ROOT}/master-thesis-backend-sequencer"
export REPO_LAYOUT="${SRC_ROOT}/master-thesis-backend-layout"
export REPO_PROTO="${SRC_ROOT}/master-thesis-protorepo"
export REPO_AUTOFILL="${SRC_ROOT}/master-thesis-autofill"

export IMAGE_VERSION='latest'

export IMAGE_JAVA='dago-backend-java'
export IMAGE_RUST='dago-backend-rust'
export IMAGE_CPP='dago-backend-cpp'
export IMAGE_WASM='dago-wasm'
export IMAGE_FRONT_PROXY='dago-envoy'
export IMAGE_DATABASE='dago-postgres'
export IMAGE_CACHE='dago-redis'

export DOCKER='../docker'
export DOCKER_FRONTEND="${REPO_FRONTEND}/docker"
export DOCKER_BACKEND="${REPO_BACKEND}/docker"
export DOCKER_PREPROCESSOR="${REPO_PREPROCESSOR}/docker"
export DOCKER_SEQUENCER="${REPO_SEQUENCER}/docker"
export DOCKER_LAYOUT="${REPO_LAYOUT}/docker"
export DOCKER_AUTOFILL="${REPO_AUTOFILL}/docker"
export DOCKER_FRONT_PROXY="${DOCKER}/envoy"
export DOCKER_DATABASE="${DOCKER}/postgres"
export DOCKER_CACHE="${DOCKER}/redis"

export FRONTEND_HOST='dago-frontend'
export FRONTEND_PORT=8080

export BACKEND_HOST='dago-backend'
export BACKEND_PORT=9090

export PREPROCESSOR_HOST='dago-preprocessor'
export PREPROCESSOR_PORT=9090

export SEQUENCER_HOST='dago-sequencer'
export SEQUENCER_PORT=9090

export LAYOUT_HOST='dago-layout'
export LAYOUT_PORT=9090

export FRONT_PROXY_HOST='dago-front-proxy'
export FRONT_PROXY_PORT='9080'

export POSTGRES_HOST='postgres'
export POSTGRES_PORT=5432
export POSTGRES_DB='dago'
export POSTGRES_USER='dago'
export POSTGRES_PASSWORD='dago'

export REDIS_HOST='redis'
export REDIS_PORT=6379

export VOLUME_PROJECT='/home/dago'
export VOLUME_PROTO='/home/proto'

popd > /dev/null
