#!/usr/bin/env bash
set -o nounset
#set -o xtrace

cd $(dirname "$0")
source ./config.sh

# Start and add dago-layout individually.
# Parameters like runtime are currently not supported by docker-compose
# which makes it necessary to manage the container manually.
docker run -it -d \
  --name dago-autofill \
  --volume "${REPO_AUTOFILL}/":"${VOLUME_PROJECT}":rw \
  --volume "${REPO_PROTO}/":"${VOLUME_PROTO}":ro \
  --network dago-net \
  --env LOG_LEVEL='INFO' \
  --env BACKEND_HOST="${BACKEND_HOST}" \
  --env BACKEND_PORT="${BACKEND_PORT}" \
  --env VOLUME_PROTO="${VOLUME_PROTO}" \
  "${IMAGE_RUST}:${IMAGE_VERSION}"

# View logs.
docker container logs -f dago-autofill
