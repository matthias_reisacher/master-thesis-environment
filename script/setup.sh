#!/usr/bin/env bash
set -o nounset
#set -o xtrace

cd $(dirname "$0")
source config.sh

die() {
    echo "$@" >&2
    exit 1
}

build_image() {
    docker image build -t $1 $2
}

# Check if all required repositories can be found
[[ ! -d ${REPO_FRONTEND} ]] && die "Error: Could not find '${REPO_FRONTEND}'"
[[ ! -d ${REPO_BACKEND} ]] && die "Error: Could not find '${REPO_BACKEND}'"
[[ ! -d ${REPO_PREPROCESSOR} ]] && die "Error: Could not find '${REPO_PREPROCESSOR}'"
[[ ! -d ${REPO_LAYOUT} ]] && die "Error: Could not find '${REPO_LAYOUT}'"
[[ ! -d ${REPO_PROTO} ]] && die "Error: Could not find '${REPO_PROTO}'"

# Stop all running instances
docker container rm -f dago-layout 2> /dev/null
docker container rm -f dago-autofill 2> /dev/null
docker-compose -f "${DOCKER}/docker-compose.yml" down

# Build images
build_image "${IMAGE_WASM}:${IMAGE_VERSION}" "${DOCKER_FRONTEND}"
build_image "${IMAGE_JAVA}:${IMAGE_VERSION}" "${DOCKER_BACKEND}"
build_image "${IMAGE_RUST}:${IMAGE_VERSION}" "${DOCKER_PREPROCESSOR}"
build_image "${IMAGE_CPP}:${IMAGE_VERSION}" "${DOCKER_LAYOUT}"
build_image "${IMAGE_FRONT_PROXY}:${IMAGE_VERSION}" "${DOCKER_FRONT_PROXY}"
build_image "${IMAGE_DATABASE}:${IMAGE_VERSION}" "${DOCKER_DATABASE}"
build_image "${IMAGE_CACHE}:${IMAGE_VERSION}" "${DOCKER_CACHE}"

# Initialize database
docker-compose -f "${DOCKER}/docker-compose.yml" up -d database
sleep 10
./init_postgres.sh
docker-compose -f "${DOCKER}/docker-compose.yml" stop database
