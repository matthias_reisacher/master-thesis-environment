#!/usr/bin/env bash
set -o nounset
#set -o xtrace

cd $(dirname "$0")
source config.sh

docker container cp ../docker/postgres/init.sql database:/root/

docker container exec -it database /bin/bash -c "\
	export PGPASSWORD=${POSTGRES_PASSWORD} ; \
	psql \
		-X \
		-d ${POSTGRES_DB} \
		-U ${POSTGRES_USER} \
		-f /root/init.sql \
		--echo-all \
		--set ON_ERROR_STOP=on ; \
	PSQL_EXIT_STATUS=\$? ; \
	if [ \${PSQL_EXIT_STATUS} -ne 0 ]; then \
		echo 'Error: psql failed while trying to run sql-init script' 1>&2
	fi
	exit \${PSQL_EXIT_STATUS}"

DOCKER_RES=$?
if [ ${DOCKER_RES} -eq 0 ]; then
	echo "Initialized database successfully"
fi

exit ${DOCKER_RES}
