#!/usr/bin/env bash
set -o nounset
#set -o xtrace

cd $(dirname "$0")
source ./config.sh

# Start containers in background
docker-compose -f "${DOCKER}/docker-compose.yml" up -d

# Due to some container runtime problem, the dago-layout container cannot be restarted.
# Therefore, recreate the container instead.
docker container rm -f dago-layout 2> /dev/null

# Start and add dago-layout individually.
# Parameters like runtime are currently not supported by docker-compose
# which makes it necessary to manage the container manually.
docker run -it -d \
  --name dago-layout \
  --runtime=nvidia \
  --ulimit core=-1 \
  --security-opt seccomp=unconfined \
  --volume "${REPO_LAYOUT}/":"${VOLUME_PROJECT}":rw \
  --volume "${REPO_PROTO}/":"${VOLUME_PROTO}":ro \
  --network dago-net \
  --network-alias "${LAYOUT_HOST}" \
  --expose "${LAYOUT_PORT}" \
  --env GRPC_PORT="${LAYOUT_PORT}" \
  --env PREPROCESSOR_HOST="${PREPROCESSOR_HOST}" \
  --env PREPROCESSOR_PORT="${PREPROCESSOR_PORT}" \
  --env REDIS_HOST="${REDIS_HOST}" \
  --env REDIS_PORT="${REDIS_PORT}" \
  --env VOLUME_PROTO="${VOLUME_PROTO}" \
  "${IMAGE_CPP}:${IMAGE_VERSION}"

# View logs.
# Note: logs from dago-layout are not shown!
docker-compose -f "${DOCKER}/docker-compose.yml" logs -f
