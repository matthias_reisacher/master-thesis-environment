CREATE TABLE graph (
    id          BIGSERIAL       PRIMARY KEY,
    name        VARCHAR(60)     NOT NULL,
    description VARCHAR(150)    NOT NULL,
    timestamp   TIMESTAMP       NOT NULL
);

CREATE TABLE page (
    id          BIGSERIAL   PRIMARY KEY,
    graph_id    BIGINT      REFERENCES graph(id),
    number      SMALLINT    NOT NULL CHECK (number>=0),
    max_depth   SMALLINT    NOT NULL CHECK (max_depth>=0),
    timestamp   TIMESTAMP   NOT NULL
);

CREATE TABLE position (
    id  BIGSERIAL   PRIMARY KEY,
    x   REAL        NOT NULL,
    y   REAL        NOT NULL
);

CREATE TABLE attribute (
    id          BIGSERIAL   PRIMARY KEY,
    data_id     BIGINT,
    quad_tree   BIGINT      NOT NULL CHECK (quad_tree >= 0),
    attributes  JSON        NOT NULL,
    lifetime    SMALLINT    NOT NULL
);

CREATE TABLE node (
    id              BIGSERIAL   PRIMARY KEY,
    page_id         BIGINT      REFERENCES page(id),
    depth           SMALLINT    NOT NULL CHECK (depth>=0),
    leaf            BOOL        NOT NULL,
    position_id     BIGINT      REFERENCES position(id),
    attribute_id    BIGINT      REFERENCES attribute(id)
);

CREATE TABLE edge (
    id              BIGSERIAL   PRIMARY KEY,
    page_id         BIGINT      REFERENCES page(id),
    depth           SMALLINT    NOT NULL CHECK (depth>=0),
    start_node_id   BIGINT      REFERENCES position(id),
    end_node_id     BIGINT      REFERENCES position(id),
    attribute_id    BIGINT      REFERENCES attribute(id)
);